<?php

namespace App\Http\Controllers\Admin\Answers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Answer;
use App\Traits\HasApiResponses;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class AnswerController extends Controller
{
    use HasApiResponses;

    public function setAnswerForQuestion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'A' => 'required|string',
            'B' => 'required|string',
            'C' => 'required|string',
            'D' => 'required|string',
            'question_id' => 'required|exists:questions,id',
            'correct_answer' => 'required',
            'correct_answer' => Rule::in(['A', 'B', 'C', 'D']),
        ]);

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $correct_answer = $request->correct_answer;
        $answers = $request->only(['A', 'B', 'C', 'D']);

        foreach ($answers as $answer => $value) {
            Answer::create([
                'answer' => $value,
                'question_id' => $request->question_id,
                'correct_answer' => $correct_answer == $answer ? 1 : 0,
            ]);
        }


        return $this->successResponse("Answers successfully saved");
    }

    public function getAnswersForQuestion($id)
    {
        $answers = Answer::whereQuestionId($id)->get();
        return $this->successResponse("All answers for the Question", $answers);
    }

    public function updateAnswerToQuestion(Request $request, $answerId)
    {
        $validator = Validator::make($request->only(['answer', 'question_id', 'correct_answer']), [
            'answer' => 'required',
            'question_id' => 'required|exists:questions,id',
            'correct_answer' => 'in:0,1',
        ]);

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $answerToUpdate = Answer::findOrFail($answerId);
        $correctAnswer = $request->correct_answer;

        $answerToUpdate->answer = $request->answer;

        if ($correctAnswer) {
            $answersCorrect = Answer::whereQuestionId($request->question_id)->whereCorrectAnswer(1)->get();
            if ($answersCorrect->isEmpty()) {
                $answerToUpdate->correct_answer = 1;
                $answerToUpdate->save();
            }
            return $this->badRequestAlert("There is a correct answer already");
        }

        $answerToUpdate->correct_answer = 0;
        $answerToUpdate->save();

        return $this->successResponse("Answer successfully updated");

    }
}
