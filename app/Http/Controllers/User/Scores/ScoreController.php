<?php

namespace App\Http\Controllers\User\Scores;

use App\Http\Controllers\Controller;
use App\Models\Score;
use Illuminate\Http\Request;
use App\Traits\HasApiResponses;

class ScoreController extends Controller
{
    use HasApiResponses;

    public function setScore(Request $request)
    {
        $request->validate(
            [
                'user_id' => 'required|integer|exists:users,id',
                'course_id' => 'required|integer|exists:courses,id',
                'score' => 'required|integer',
            ]
        );

        $score = Score::create($request->all());

        return $this->successResponse($score);

    }

    public function getAllScores($userId)
    {
        $scores = Score::whereUserId($userId)->get();

        return $this->successResponse($scores);
    }

    public function getAllScoresByCourse($courseId)
    {
        $scores = Score::whereCourseId($courseId)->get();

        return $this->successResponse($scores);
    }
}
