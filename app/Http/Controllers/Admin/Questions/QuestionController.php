<?php

namespace App\Http\Controllers\Admin\Questions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Traits\HasApiResponses;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    use HasApiResponses;

    public function getAllQuestionsAndCourses()
    {
        $questions = Question::whereStatus('active')->get()->load('course');
        return $this->successResponse('All Active Questions and Courses', $questions);
    }

    public function getAllQuestions()
    {
        $questions = Question::whereStatus('active')->get();
        return $this->successResponse('All Active Questions', $questions);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'admin_id' => 'required|exists:admins,id',
                'question' => 'required|max:500',
                'course_id' => 'required|integer',
            ],
            [
                'admin_id.exists' => 'The selected Admin ID does not exist'
            ]
        );

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $question = Question::create($request->only(['admin_id', 'question', 'course_id']));
        return $this->createdResponse("Question successfully created", $question);
    }

    public function getQuestion($id)
    {
        $question = Question::whereStatus('active')->findOrFail($id);
        return $this->successResponse('Showing Question', $question);
    }

    public function getQuestionAndAnswers($id)
    {
        $question = Question::whereStatus('active')->findOrFail($id);
        $answers = $question->answersToQuestion();

        $data = [
            'question' => $question,
            'answers' => $answers,
        ];

        return $this->successResponse('Showing Question', $data);
    }

    public function getQuestionAndCourse($id)
    {
        $question = Question::whereStatus('active')->findOrFail($id)->load('course');
        return $this->successResponse('Showing Question', $question);
    }

    public function updateQuestion(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'question' => 'required|max:500',
            'course_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $question = Question::findOrFail($id);
        $question->update($request->only(['question', 'course_id']));

        return $this->successResponse("Question successfully updated", $question);
    }

    public function inactiveQuestion($id)
    {
        $question = Question::find($id);
        $question->update(['status' => 'inactive']);

        return $this->successResponse("Question inactivated", $question);
    }

    public function activeQuestion($id)
    {
        $question = Question::find($id);
        $question->update(['status' => 'active']);

        return $this->successResponse("Question activated", $question);
    }

    public function getAllInactiveQuestions()
    {
        $questions = Question::whereStatus('inactive')->get();
        return $this->successResponse("All Inactive Questions", $questions);
    }

    public function getAllInactiveQuestionsAndAnswers()
    {
        $questions = Question::whereStatus('inactive')->get()->load('course');
        return $this->successResponse("All Inactive Questions and Answers", $questions);
    }

    public function getQuestionsByAdminId($id)
    {
        $questions = Question::whereAdminId($id)->get();

        return $this->successResponse("All Questions by the Admin", $questions);
    }
}
