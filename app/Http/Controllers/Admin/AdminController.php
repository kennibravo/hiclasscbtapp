<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Traits\HasApiResponses;

class AdminController extends Controller
{
    use HasApiResponses;
    /**
     * Store a newly created user in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $token = null;

        try {
            if (!$token = JWTAuth::attempt(['email' => $request->email, 'password' => $request->password])) {
                return $this->formValidationErrorAlert('Invalid email or password');
            }
        } catch (JWTException $e) {
            return $this->badRequestAlert('Failed to create token');
        }
        return $this->successResponse('Logged in', ['token' => $token]);
    }

    public function getuser()
    {
        $user = Auth::user();

        return response()->json([
            'response' => 'success',
            'data' => $user
        ], 200);
    }

    public function logout()
    {
        auth('admin')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
}
