<?php

namespace App\Http\Controllers\User\Questions;

use App\Http\Controllers\Controller;
use App\Models\Question;
use Illuminate\Http\Request;
use App\Traits\HasApiResponses;

class QuestionController extends Controller
{
    use HasApiResponses;

    public function getQuestionsByCourse($courseId)
    {
        $questions = Question::getByCourse($courseId);
        return $this->successResponse($questions);
    }

    public function getQuestionsByAdmin($adminId)
    {
        $questions = Question::getByAdmin($adminId);
        return $this->successResponse($questions);
    }
}
