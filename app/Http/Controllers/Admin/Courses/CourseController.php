<?php

namespace App\Http\Controllers\Admin\Courses;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Traits\HasApiResponses;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    use HasApiResponses;

    public function getAllCourses()
    {
        $courses = Course::all()->load('admin');
        return $this->successResponse("All Courses", $courses);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|max:10',
                'admin_id' => 'required|exists:admins,id',
            ],
            [
                'admin_id.exists' => 'The selected Admin ID does not exist'
            ]
        );

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $question = Course::create($request->only(['name', 'admin_id']));
        return $this->successResponse("Course successfully created", $question);
    }

    public function getCourse($id)
    {
        $course = Course::findOrFail($id)->load('admin');
        return $this->successResponse("Showing Question", $course);
    }

    public function updateCourse(Request $request, $id)
    {
        $validator = Validator::make($request->only(['name']), [
            'name' => 'required|max:10'
        ]);

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $question = Course::findOrFail($id);
        $question->update($request->only(['name']));

        return $this->successResponse("Course successfully updated", $question);
    }
}
