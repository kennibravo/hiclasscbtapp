<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateApiKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_keys', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->timestamps();
        });

        $keys = [
            'B3uQDrMZ5Jr3kMeaKC39',
            'y61HYJNIb6L5vUX3oimR',
            '0ptXAC7pS1kMhmrdNCxS',
            'O8Tk9qB8R3Z2Sfx696cn'
        ];

        for ($i = 0; $i <= 3; $i++) {
            DB::table('api_keys')->insert(
                [
                    'key' => $keys[$i]
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('api_keys');
    }
}
