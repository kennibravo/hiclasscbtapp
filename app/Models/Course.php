<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = ['updated_at'];

    public function questions()
    {
        return $this->hasMany('App\Models\Question')->withDefault();
    }

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin')->withDefault();
    }

    public static function searchCourseName($data)
    {
        return self::whereOneLike('name', $data);
    }

    public static function searchMulti($data)
    {
        return self::whereMultiLike(['name', 'admin_id'], $data);
    }

    public function score()
    {
        return $this->hasMany('App\Models\Score');
    }
}
