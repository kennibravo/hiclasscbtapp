<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['name' => 'user.', 'prefix' => 'user', 'namespace' => 'User', 'middleware' => ['assign.guard:web']], function () {
    Route::post('register', 'UserController@register')->name('user.register');
    Route::post('login', 'UserController@login')->name('user.login');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::group(['prefix' => 'profile'], function () {
            Route::get('{id}', 'ProfileController@getProfileDetails')->name('profile.get');
            Route::post('store', 'ProfileController@createUserProfile')->name('profile.create');
            Route::put('update/{id}', 'ProfileController@editProfileDetails')->name('profile.edit');
            Route::put('update-password/{id}', 'ProfileController@changePassword')->name('profile.password');
        });

        Route::group(['prefix' => 'questions', 'namespace' => 'Questions'], function () {
            Route::get('course/{courseId}', 'QuestionController@getQuestionsByCourse')->name('questions.get.course');
            Route::get('admin/{adminId}', 'QuestionController@getQuestionsByAdmin')->name('questions.get.admin');
        });

        Route::group(['name' => 'courses.', 'prefix' => 'courses', 'namespace' => 'Courses'], function () {
           Route::get('/all', 'CourseController@getAllCourses')->name('index');
           Route::get('/search/{courseName}', 'CourseController@searchSingleCourseName')->name('search.name');
        });

        Route::group(['name' => 'scores.', 'prefix' => 'scores', 'namespace' => 'Scores'], function () {
           Route::post('store', 'ScoreController@setScore')->name('store');
        });
    });

});

Route::group(['name' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['assign.guard:admin']], function () {
    Route::post('login', 'AdminController@login')->name('admin.login');

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::group(['prefix' => 'questions', 'namespace' => 'Questions'], function () {
            Route::get('courses/all', 'QuestionController@getAllQuestionsAndCourses')->name('questions.courses.all');
            Route::get('all', 'QuestionController@getAllQuestions')->name('questions.all');
            Route::post('store', 'QuestionController@store')->name('questions.store');
            Route::get('{id}', 'QuestionController@getQuestion')->name('questions.show');
            Route::get('answers/{id}', 'QuestionController@getQuestionAndAnswers')->name('questions.answers.show');
            Route::get('course/{id}', 'QuestionController@getQuestionAndCourse')->name('question.course.show');
            Route::put('update/{id}', 'QuestionController@updateQuestion')->name('questions.update');
            Route::get('inactive/all', 'QuestionController@getAllInactiveQuestions')->name('questions.all.inactive');
            Route::get('answers-inactive/all', 'QuestionController@getAllInactiveQuestionsAndAnswers')->name('questions.answers.inactive.all');
            Route::get('inactive/{id}', 'QuestionController@inactiveQuestion')->name('questions.inactive');
            Route::get('active/{id}', 'QuestionController@activeQuestion')->name('questions.active');
            Route::get('admin-id/{id}', 'QuestionController@getQuestionsByAdminId')->name('questions.get.admin-id');

            Route::group(['prefix' => 'utils'], function () {
                Route::get('search/{search}', 'QuestionSearchController@searchSingleQuestion')->name('questions.utils.search');
                Route::get('multi-search/{search}', 'QuestionSearchController@searchMultipleData')->name('questions.utils.multi-search');
            });
        });

        Route::group(['prefix' => 'courses', 'namespace' => 'Courses'], function () {
            Route::get('all', 'CourseController@getAllCourses')->name('courses.all');
            Route::post('store', 'CourseController@store')->name('courses.store');
            Route::get('{id}', 'CourseController@getCourse')->name('courses.show');
            Route::put('update/{id}', 'CourseController@updateCourse')->name('courses.update');

            Route::group(['prefix' => 'utils'], function () {
                Route::get('search/{course}', 'CourseSearchController@searchSingleCourse')->name('courses.utils.search');
                Route::get('multi-search/{course}', 'CourseSearchController@searchSingleCourse')->name('courses.utils.multi-search');
            });
        });

        Route::group(['prefix' => 'answers', 'namespace' => 'Answers'], function () {
            Route::get('/{questionId}/all', 'AnswerController@getAnswersForQuestion')->name('answers.all');
            Route::post('/set', 'AnswerController@setAnswerForQuestion')->name('answers.question.set');
            Route::put('/update/{answerId}', 'AnswerController@updateAnswerToQuestion')->name('answers.question.update');
        });
    });
});
