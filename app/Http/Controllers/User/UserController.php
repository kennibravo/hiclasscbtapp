<?php

namespace App\Http\Controllers\User;

use JWTAuth;
use App\Models\User;
use JWTAuthException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Traits\HasApiResponses;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    use HasApiResponses;

    /**
     * Store a newly created user in database.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:8|confirmed'
        ]);

        if ($validator->fails()) {
           return $this->formValidationErrorAlert($validator->errors());
        }

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return $this->successResponse("User successfully registered");

    }

    /**
     * Login user with valid credentails.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $token = null;

        try {
		    if (!$token = JWTAuth::attempt(['email' => $request->email, 'password' => $request->password])) {
		        return $this->formValidationErrorAlert("Invalid Credentials");
		    }
		} catch (JWTAuthException $e) {
		    return $this->badRequestAlert("Failed to create token");
		}
		return $this->successResponse('Login successful', $token);
    }



    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */

     public function getUser()
     {
        $user = Auth::user();

        return $this->successResponse('User details', $user);
     }

    public function logout()
    {
        auth()->logout();
        return $this->successResponse("User logged out successfully");
    }


}
