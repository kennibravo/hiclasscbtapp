<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserProfile;
use App\Traits\HasApiResponses;
use Hash;
use Illuminate\Http\Request;
use Validator;

class ProfileController extends Controller
{
    use HasApiResponses;

    public function getProfileDetails($userId)
    {
        $profile = User::find($userId)->load('profile');

        return $this->successResponse("Profile Details", $profile);

    }

    public function createUserProfile(Request $request)
    {
        $rules = [
            'user_id' => 'required|unique:user_profiles',
            'bio' => 'required|max:200',
            'dob' => 'required|max:10',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        UserProfile::create([
            'user_id' => $request->user_id,
            'bio' => $request->bio,
            'dob' => $request->dob,
        ]);

        return $this->successResponse("Profile Successfully created");
    }

    public function editProfileDetails(Request $request, $userId)
    {
        $rules = [
            'bio' => 'required|max:200',
            'dob' => 'required|max:10',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $userProfile = UserProfile::where('user_id', $userId)->first();
        if ($userProfile != null) {
            $userProfile->fill($request->all())->save();
            return $this->successResponse("Profile updated successfully");
        }

    }

    public function changePassword(Request $request, $id)
    {
        $rules = [
            'password' => 'required|min:6|confirmed',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->formValidationErrorAlert($validator->errors());
        }

        $user = User::whereId($id)->first();
        if ($user != NULL) {
            $newPassword = Hash::make($request->password);
            $user->password = $newPassword;

            $user->save();
            return $this->successResponse("Password updated successfully");
        } else {
            return $this->notFoundAlert("User cannot be found");
        }
    }
}
