<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $guarded = ['created_at'];
    protected $hidden = ['created_at', 'updated_at', 'id'];

    public function question()
    {
        return $this->belongsTo('App\Models\Question')->withDefault();
    }
}
