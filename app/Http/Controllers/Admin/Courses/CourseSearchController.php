<?php

namespace App\Http\Controllers\Admin\Courses;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Traits\HasApiResponses;

class CourseSearchController extends Controller
{
    use HasApiResponses;

    public function searchSingleCourse($search)
    {
        $courses = Course::searchCourseName($search)->get();

        return $this->successResponse("Searched Course Result", $courses);
    }

    public function searchMultipleData($search)
    {
        $courses = Course::searchMulti($search)->get();

        return $this->successResponse("Searched Data in Courses", $courses);
    }
}
