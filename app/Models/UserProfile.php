<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['created_at', 'updated_at', 'id'];

    public function user(){
        return $this->belongsTo('App\Models\User'   );
    }
}
