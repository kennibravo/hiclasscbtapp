<?php

namespace App\Http\Middleware;

use App\Models\ApiKey;
use App\Traits\HasApiResponses;
use Closure;
use Illuminate\Http\Request;

class CheckIfValidApiKey
{
    use HasApiResponses;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = $request->headers->get('access-token');
        $apiKey = ApiKey::where('key', $key)->first();

        if (is_null($apiKey) || is_null($key)) {
            return $this->badRequestAlert("Please supply valid API Key");
        }

        return $next($request);
    }
}
