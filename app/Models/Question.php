<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = ['updated_at'];
    protected $appends = ['correct_answer'];

    public function course()
    {
        return $this->belongsTo('App\Models\Course')->withDefault();
    }

    public function admin()
    {
        return $this->belongsTo('App\Models\Admin')->withDefault();
    }

    public static function searchSingleQuestion($data)
    {
        return self::whereOneLike('question', $data);
    }

    public static function searchMulti($data)
    {
        return self::whereMultiLike(['question', 'status', 'admin_id'], $data);
    }

    public function answersToQuestion()
    {
        return Answer::whereQuestionId($this->id)->get();
    }

    public function getCorrectAnswerAttribute()
    {
        return Answer::whereQuestionId($this->id)->whereCorrectAnswer(1)->first();
    }

    public static function getByCourse($courseId)
    {
        return self::whereCourseId($courseId)->get();
    }

    public static function getByAdmin($adminId)
    {
        return self::whereAdminId($adminId)->get();
    }
}
