<?php

namespace App\Http\Controllers\Admin\Questions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Traits\HasApiResponses;

class QuestionSearchController extends Controller
{
    use HasApiResponses;

    public function searchSingleQuestion($search)
    {
        $questions = Question::searchSingleQuestion($search)->get();

        return $this->successResponse("Searched Question Result", $questions);
    }

    public function searchMultipleData($search)
    {
        $questions = Question::searchMulti($search)->get();

        return $this->successResponse("Searched Data in Questions", $questions);
    }
}
