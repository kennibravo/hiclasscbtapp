<?php

namespace App\Http\Controllers\User\Courses;

use App\Http\Controllers\Controller;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Traits\HasApiResponses;

class CourseController extends Controller
{
    use HasApiResponses;

    public function getAllCourses()
    {
        $courses = Course::all();

        return $this->successResponse($courses);
    }

    public function searchSingleCourseName($courseName)
    {
        $courses = Course::whereName($courseName)->get();

        return $this->successResponse($courses);
    }
}
