<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['updated_at', 'id'];

    public function course()
    {
        return $this->belongsTo('App\Models\Course')->withDefault();
    }
}
